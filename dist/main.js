/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _initIndexedDB__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./initIndexedDB */ \"./src/initIndexedDB.js\");\n/* harmony import */ var _initIndexedDB__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_initIndexedDB__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _styles_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styles.css */ \"./src/styles.css\");\n/* harmony import */ var _styles_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_styles_css__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\nif ('serviceWorker' in navigator) {\n  window.addEventListener('load', function () {\n    navigator.serviceWorker.register('../sw.js').then(function (registration) {\n      console.log('Worker registration successful in scope ', registration.scope);\n    }, function (err) {\n      console.log('Worker registration failed');\n      console.log('err:', err);\n    })[\"catch\"](function (err) {\n      console.log('err: ', err);\n    });\n  });\n  var installButton = document.getElementById(\"installButton\");\n  installButton.style.display = \"none\";\n  window.addEventListener('beforeinstallprompt', function (event) {\n    event.preventDefault();\n    console.log('before install prompt');\n    window.deferredPrompt = event;\n    installButton.style.display = \"inline-block\";\n    console.log('install button shown');\n  });\n  installButton.addEventListener('click', function (event) {\n    console.log('hide button');\n    installButton.style.display = \"none\";\n    console.log(window.deferredPrompt);\n    var promptEvent = window.deferredPrompt;\n\n    if (!promptEvent) {\n      console.log('no prompt event');\n      return;\n    }\n\n    promptEvent.prompt();\n    promptEvent.userChoice.then(function (choiceResult) {\n      if (choiceResult.outcome === \"accepted\") {\n        console.log(\"User accepted A2HS\");\n      } else {\n        console.log(\"User dismissed A2HS\");\n      }\n\n      promptEvent = null;\n      window.deferredPrompt = null;\n    });\n  });\n} else {\n  console.log('Service Worker is not supported by browser.');\n}\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/initIndexedDB.js":
/*!******************************!*\
  !*** ./src/initIndexedDB.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// init indexedDB\nvar request = indexedDB.open('offline_db', 1);\nvar db;\n\nrequest.onupgradeneeded = function () {\n  db = request.result; // init a \"collection\"\n\n  var store = db.createObjectStore('steps', {\n    keyPath: 'id'\n  });\n  store.add({\n    id: 1,\n    name: \"La formation des grottes\",\n    audio: '.mp3',\n    description: ''\n  });\n}; // when request succeed\n\n\nrequest.onsuccess = function () {\n  db = request.result;\n};\n\nrequest.onblocked = function () {\n  // if other tabs active with db, need to be closed\n  console.log('Please close all other tabs with this site open');\n};\n\n//# sourceURL=webpack:///./src/initIndexedDB.js?");

/***/ }),

/***/ "./src/styles.css":
/*!************************!*\
  !*** ./src/styles.css ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./src/styles.css?");

/***/ })

/******/ });