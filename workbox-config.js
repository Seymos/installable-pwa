module.exports = {
  "globDirectory": "src/",
  "globPatterns": [
    "**/*.{js,png,html,json,css}"
  ],
  "swSrc": "src/build-sw.js",
  "swDest": "src/sw.js",
  "templatedURLs": {
    '/': 'src/index.html',
  }
};