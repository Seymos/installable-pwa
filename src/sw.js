importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

workbox.precaching.precacheAndRoute([{"revision":"189df353c2b111169a807efb7bc194dd","url":"index.js"},{"revision":"6fa1097b8b1c6c2a0ad82fd20fa69722","url":"assets/navbar-logo-zoom.png"},{"revision":"f164b7dc4308de40e321e03b87b1d1de","url":"assets/navbar-logo.png"},{"revision":"9716c55ee9e15e6bdab47746df3cdcbe","url":"assetsToCache.js"},{"revision":"cb9258c2e20dcdafead996a8c7a68a0c","url":"images/icons-192.png"},{"revision":"134191b4d3c01517852950cacd8bfcf6","url":"images/icons-512.png"},{"revision":"6fa1097b8b1c6c2a0ad82fd20fa69722","url":"images/navbar-logo-zoom.png"},{"revision":"f164b7dc4308de40e321e03b87b1d1de","url":"images/navbar-logo.png"},{"revision":"7297996767998b3026056eabdae6f801","url":"index.html"},{"revision":"c87c1849ba3141afcf3bf6decb4978b7","url":"initIndexedDB.js"},{"revision":"5c7ae8176848d60dff3c3b68d49d887f","url":"manifest.json"},{"revision":"944b3ecf9457167a1586acf3ee1a42a7","url":"styles.css"},{"revision":"d1eebb15402593fe3c857b01a2902c32","url":"sw-old.js"},{"revision":"e249faefed5757034596c5096d33dab6","url":"/"}]);
workbox.precaching.precacheAndRoute([]);

workbox.routing.registerRoute(
    'https://use.fontawesome.com/releases/v5.3.1/js/all.js',
    new workbox.strategies.CacheFirst(),
);

workbox.routing.registerRoute(
    /\.(?:json)$/,
    new workbox.strategies.CacheFirst(),
);

workbox.routing.registerRoute(
    /\.(?:mp3|m4a)$/,
    new workbox.strategies.CacheFirst({
        cacheName: 'audio',
        plugins: [
            new workbox.expiration.Plugin({
                maxEntries: 100,
                maxAgeSeconds: 30 * 24 * 60 * 60,
            }),
        ],
    }),
);