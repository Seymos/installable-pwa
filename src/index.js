import "./initIndexedDB";
import './styles.css';

if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
        navigator.serviceWorker.register('../sw.js').then(function(registration) {
        console.log('Worker registration successful in scope ', registration.scope);
        }, function(err) {
        console.log('Worker registration failed');
        console.log('err:',err);
        }).catch(function(err) {
        console.log('err: ',err);
    });
});

const installButton = document.getElementById("installButton");
installButton.style.display = "none";

window.addEventListener('beforeinstallprompt', function(event) {
    event.preventDefault();
    console.log('before install prompt');
    window.deferredPrompt = event;
    installButton.style.display = "inline-block";
    console.log('install button shown');
});

installButton.addEventListener('click', function (event) {
    console.log('hide button');
    installButton.style.display = "none";
    console.log(window.deferredPrompt);
    let promptEvent = window.deferredPrompt;
    if(!promptEvent) {
    console.log('no prompt event');
    return;
    }
    promptEvent.prompt();

    promptEvent.userChoice.then(
    (choiceResult) => {
        if(choiceResult.outcome === "accepted") {
        console.log("User accepted A2HS");
        } else {
        console.log("User dismissed A2HS");
        }
        promptEvent = null;
        window.deferredPrompt = null;
    }
    );
})
} else {
console.log('Service Worker is not supported by browser.');
}