// init indexedDB
const request = indexedDB.open('offline_db', 1);

let db;

request.onupgradeneeded = function() {
    db = request.result;

    // init a "collection"
    const store = db.createObjectStore('steps', { keyPath: 'id'});
    store.add({
        id: 1,
        name: "La formation des grottes",
        audio: '.mp3',
        description: ''
    });
};

// when request succeed
request.onsuccess = function() {
    db = request.result;
};

request.onblocked = function() {
    // if other tabs active with db, need to be closed
    console.log('Please close all other tabs with this site open');
};