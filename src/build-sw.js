importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

workbox.precaching.precacheAndRoute(self.__WB_MANIFEST);
workbox.precaching.precacheAndRoute([]);

workbox.routing.registerRoute(
    'https://use.fontawesome.com/releases/v5.3.1/js/all.js',
    new workbox.strategies.CacheFirst(),
);

workbox.routing.registerRoute(
    /\.(?:json)$/,
    new workbox.strategies.CacheFirst(),
);

workbox.routing.registerRoute(
    /\.(?:png|jpg)$/,
    new workbox.strategies.CacheFirst({
        cacheName: 'image',
        plugins: [
            new workbox.expiration.Plugin({
                maxEntries: 100,
                maxAgeSeconds: 30 * 24 * 60 * 60,
            }),
        ],
    }),
);