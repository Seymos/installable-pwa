export default [
    '/styles.css',
    '/index.js',
    '/manifest.json',
    '/index.html',
    'https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css',
    'https://use.fontawesome.com/releases/v5.3.1/js/all.js'
];

export const cacheWhiteList = ['pages-cache-v1', 'blog-post-cache-v1'];